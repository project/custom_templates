<?php

/**
 * @file
 *   Include file
 *
 * @version
 *
 * @developers:
 *    Rafal Wieczorek <kenorb@gmail.com>
 */

/**
 * Build list of templates for user
 *
 * @param object $uid user id (get current if empty)
 * @param return array of current templates (customized templates are overriden)
 */
function custom_templates_load_template_nodes($uid = NULL) {
  if (empty($uid)) {
    global $user;
    $uid = $user->uid;
  }
  $templates = array();
  $db_result = db_query("SELECT nid, title FROM {node} WHERE type = '%s' AND ((title LIKE '%s%%' AND uid = %d) OR (title NOT LIKE '%s%%')) ORDER BY nid", TEMPLATE_CT_NAME, TEMPLATE_CT_NAME, $uid, TEMPLATE_CT_NAME, $uid);
  while ($row = db_fetch_object($db_result)) {
    $key = TEMPLATE_CT_NAME."|$uid";
    if (substr($row->title, 0, strlen(TEMPLATE_CT_NAME)) == TEMPLATE_CT_NAME) {
      list($template, $uid, $nid, $node_title) = explode('|', $row->title);
      $key .= '|' . $nid;
      $node = node_load($row->nid); // load original template node
      $node->title = node_load($nid)->title;
    } else {
      $key .= '|' . $row->nid;
      $node = node_load($row->nid); // load customized template node
    }
    $templates[$key] = $node;
  }
  return $templates;
}

/**
 * Build list of templates for user
 *
 * @param object $uid user id (get current if empty)
 * @param return array of current templates (customized templates are overriden)
 */
function custom_templates_save_template_nodes($nodes = array(), $uid = NULL) {
  if (empty($uid)) {
    global $user;
  }
  $res = TRUE;
  foreach ($nodes as $template_name => $node_body) {
    list($template, $uid, $nid, $node_title) = explode('|', $template_name);
    $key = "$template|${uid}|$nid";
    if ($template == 'custom_template') { // if it's template
      if ($node = node_load(array('title' => $template_name, 'uid' => $user->uid))) { // check if user have already his own node
        $node->body = $node_body;
      } else {
        $node = node_load($nid);
        $node->uid = $user->uid; // change author to currect user
        $node->title = $key;
        $node->body = $node_body;
        unset($node->nid, $node->vid);
      }
      node_save($node);
      if (!$node->nid) {
        $res = FALSE;
      }
    }
  }
  return $res;
}

