<?php

/**
 * @file
 *   Form include file
 *
 * @version
 *
 * @developers
 *   Rafal Wieczorek <kenorb@gmail.com>
 */


define(TEMPLATE_CT_NAME, 'custom_template'); // name of the node type

/**
 * Menu callback for the user form.
 */
function custom_templates_user_form($uid) {

  $form['custom_templates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Your templates'),
    '#collapsible' => TRUE,
  ); 

  $settings = variable_get('custom_templates', array());

  $form['custom_templates']['custom_templates_form_id'] = array(
    '#type' => 'textarea',
    '#title' => t('Form IDs to personalize'),
    '#cols' => 60,
    '#rows' => 5,
    '#default_value' => variable_get('custom_templates_form_id', ''),
    '#description' => t('Type which form_id you want to personalize (one per line). Example: block_admin_display_form will personalize blocks for each user.'),
    '#wysiwyg' => FALSE,
  );

  $type_options = array('per_user' => t('Per user'), 'per_role' => t('Per role')); // default: per_user
  $form['custom_templates']['custom_templates_personalize_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Settings Type'),
    '#description' => t('Select if settings should be personalized per user or per role.'),
    '#default_value' => variable_get('custom_templates_personalize_type', array('per_user')),
    '#options' => $type_options,
  ); 

  //$form['#validate'] = array('custom_templates_admin_form_validate');

  return system_settings_form($form); 
}
 
/**
 * Form API callback to validate the settings form.
 */
function custom_templates_user_form_validate($form, &$form_state) {
  $values = &$form_state['values'];
  // TODO: trim - values['custom_templates_form_id']
  // if something then form_set_error('', t('Some error'));
} 

/**
 * Build list of user templates form.
 *
 * @param object $uid user id (get current if empty)
 */
function custom_templates_get_user_form($uid = NULL) {
  $form['custom_templates'] = array(
    '#type' => 'fieldset',
    '#title' => t('My templates'),
  );
  module_load_include('inc', 'custom_templates');
  $nodes = custom_templates_load_template_nodes();
  foreach ($nodes as $key => $node) {
    if (node_access('view', $node)) {
      $form['custom_templates'][$key] = array(
        '#title' => $node->title,
        '#type' => 'textarea',
        '#default_value' => $node->body,
      );
    }
  }
  $form['#submit'][] = 'custom_templates_get_user_form_submit';
  return $form;
}

/**
 * Submit form_alter callback
 */
function custom_templates_get_user_form_submit($form, $form_state) {
  $values = &$form_state['values'];
  module_load_include('inc', 'custom_templates');
  if (custom_templates_save_template_nodes($values)) {
    drupal_set_message(t('You templates has been saved!'));
  } else {
    drupal_set_message(t('Some error occured during saving your templates!'), 'error');
  }
}

