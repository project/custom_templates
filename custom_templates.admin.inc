<?php

/**
 * @file
 *   Form settings include file
 *
 * @version
 *
 * @developers
 *   Rafal Wieczorek <kenorb@gmail.com>
 */


/**
 * Menu callback for the settings form.
 */
function custom_templates_admin_form() {

  $form['custom_templates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form Settings'),
    '#collapsible' => TRUE,
  ); 

  $settings = variable_get('custom_templates', array());

  $form['custom_templates']['custom_templates_form_id'] = array(
    '#type' => 'textarea',
    '#title' => t('Form IDs to personalize'),
    '#cols' => 60,
    '#rows' => 5,
    '#default_value' => variable_get('custom_templates_form_id', ''),
    '#description' => t('Type which form_id you want to personalize (one per line). Example: block_admin_display_form will personalize blocks for each user.'),
    '#wysiwyg' => FALSE,
  );

  $type_options = array('per_user' => t('Per user'), 'per_role' => t('Per role')); // default: per_user
  $form['custom_templates']['custom_templates_personalize_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Settings Type'),
    '#description' => t('Select if settings should be personalized per user or per role.'),
    '#default_value' => variable_get('custom_templates_personalize_type', array('per_user')),
    '#options' => $type_options,
  ); 

  //$form['#validate'] = array('custom_templates_admin_form_validate');

  return system_settings_form($form); 
}
 
/**
 * Form API callback to validate the settings form.
 */
function custom_templates_admin_form_validate($form, &$form_state) {
  $values = &$form_state['values'];
  // TODO: trim - values['custom_templates_form_id']
  // if something then form_set_error('', t('Some error'));
} 

